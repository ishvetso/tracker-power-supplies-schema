# Tracker power supplies database schema

The schema is written in MySQL and is located at dbod-pstrackerdb.cern.ch

To connect (from lxplus, for example):

**mysql -h dbod-pstrackerdb.cern.ch -P 5501 -u admin -p**


createSchema_80.sql creates schema with 8.0.16,MySQL Community Server - GPL 

createSchema.sql tested with 10.3.23-MariaDB, MariaDB Server


Admin for the schema is through the egroup *CMS-TRK-PS-DB-admin*

REGISTERHWEXTENDED added on 25.11.2024 to allow the registraion of the new hardware with the ITEM_ID_EPOOL